import React, {useEffect, useState} from "react";
const CounterA = React.memo(({count})=>{
    useEffect(()=>{
        console.log(`counterA update - count : ${count}`)
    })
    return <div>{count}</div>
});

const CounterB = ({obj}) =>{

    useEffect(()=>{
        console.log(`counterB update - count : ${obj.count}`);
    })

    return  <div>{obj.count}</div>
};

const areEqual = (prevProps, nextProps)=>{
    if(prevProps.obj.count === nextProps.obj.count){
        return true // 이전 프롭스 현재 프롭스과 같기 때문에 리렌더링 하지 않음
    }
    return false // 이전 프롭스 현재 프롭스 다르기 때문에 리렌더링 함.
}

const MemoizedCounterB = React.memo(CounterB, areEqual);

/*    const TextView = React.memo(({text})=>{
        useEffect(()=>{
            console.log(`update :: Text : ${text}`);
        });
        return <div>{text}</div>;
    });
    const CountView = React.memo(({count})=>{
        useEffect(()=>{
            console.log(`update :: count : ${count}`);
        });
        return <div>{count}</div>;
    });

    const [count, setCount] = useState(1);
    const [text, setText] = useState("");*/
const OptimizeTest =  () => {




    const [count, setCount] = useState(1);
    const [obj, setObj]=useState({
        count : 1,
        name : 'jk',
    })




    return <div style={{padding : 50}}>
        <div style={{padding : 50}}>
            <div>
                <h2>couner A</h2>
                <CounterA count={count}></CounterA>
                <button onClick={()=>{
                    setCount(count)
                }}>A button</button>
            </div>
            <div>
                <h2>counter B</h2>
                <MemoizedCounterB obj={obj}></MemoizedCounterB>
                <button onClick={()=>setObj({
                    count : obj.count
                })}>B button</button>
            </div>
        </div>
{/*        <div>
            <h2>count</h2>
            <CountView count={count}></CountView>
            <button onClick={()=> setCount(count+1)}>+</button>
        </div>
        <div>
            <h2>text</h2>
            <TextView text={text}></TextView>
            <input value={text} onChange={(e)=>setText(e.target.value)}/>
        </div>*/}


    </div>;
};

export default OptimizeTest