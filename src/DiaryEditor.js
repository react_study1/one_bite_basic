import {useState, useRef, useEffect} from "react";
import React from "react";
import "./App.css"


const DiaryEditor = ({onCreate}) => {

    useEffect(()=>{
        //렌더링 확인용
        console.log("DiaryEditor 렌더링!");
    })

    const authorInput = useRef();
    const contentInput = useRef();

    const [state, setState] = useState({
        author : "",
        content : "",
        emotion : 1,
    });

    const handleChangeState = (e) => {
        // console.log(e.target.value);
        // console.log(e.target.name);
        setState(
            {
                ...state,
                [e.target.name] : e.target.value,

            }

        )
    }

    const handleSubmit = () => {
        if(state.author.length < 1){
            // alert("작성자는 최소 1글자 이상 입력해주세요");
            authorInput.current.focus();
            return;
        }

        if(state.content.length < 5){
            // alert("일기 본문은 최소 5글자 이상 입력해주세요");
            contentInput.current.focus();

            return;
        }

        onCreate(state.author, state.content, state.emotion);
        alert("저장성공!");

        setState({
            author: "",
            content: "",
            emotion: "",
        });
    };


    return(
    <div className="DiaryEditor">
        <h2>오늘의 일기</h2>
        <div>
            <input
                name="author"
                type="text"
                value={state.author}
                onChange={handleChangeState}
                ref={authorInput}
                />
            <div>
                <textarea
                    name="content"
                    value={state.content}
                    onChange={handleChangeState}
                    ref={contentInput}
                >

            </textarea>
            </div>

            <div>
                오늘의 감정 점수 :
                <select name="emotion" value={state.emotion} onChange={handleChangeState}>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
            </div>
            <div>
                <button onClick={handleSubmit}>버튼 저장하기</button>
            </div>
        </div>
    </div>
    )
}

export default React.memo(DiaryEditor);